import React from 'react';
import '../App.css';

function FooterCorebiz() {
  return (
    <footer className="FooterCorebiz">
      <section class="address">
        <h3>Localização</h3>
        <p>Avenida Andrômeda, 200. Bloco 6 e 8</p>
        <p>Alphavile SP</p>
        <a href="mailto:brasil@corebiz.ag">
          <p>brasil@corebiz.ag</p>
        </a>
        <a href="tel: +551130901039">
          <p>+55 11 3090 1039</p>
        </a>
      </section>
      <section class="contact">
        <ul>
          <li>
            <a href="#">
              <i class="fas fa-envelope"></i>
              <p>Entre em contato</p>
            </a>
          </li>
          <li>
            <a href="#">
              <i class="fas fa-headphones"></i>
              <p>Fale com o nosso consultor online</p>
            </a>
          </li>
        </ul>
      </section>
      <section class="madeby">
        <div>
          <p>Create by</p>
          <img src="../img/corebiz-white.png" alt="Logo Corebiz" />
        </div>
        <div>
          <p>Powered by</p>
          <img src="../img/vtex-logo.png" alt="Logo vtex" />
        </div>
      </section>
    </footer>
  );
}

export default FooterCorebiz;
