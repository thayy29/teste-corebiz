import React from 'react';
import '../App.css';

function SectionPrincipal() {
  return (
    <section className="SectionPrincipal">
      <div className="box-left">
        <div className="content-box-01">
          <img
            src="../img/bg-black-core.png"
            class="bg-img-01"
            alt="Banner principal"
          />
          <h2>Olá, o que você está buscando?</h2>
          <h1>Criar ou migrar seu e-commerce?</h1>
        </div>
      </div>

      <div className="box-right">
        <div className="content-box-02">
          <img
            src="../img/bg-core.png"
            class="bg-img-02"
            alt="Banner principal"
          />
        </div>
      </div>
    </section>
  );
}

export default SectionPrincipal;
