import React from 'react';
import '../App.css';

function Navbar() {
  return (
    <div className="container">
      <div className="Navbar">
        <div className="lefttSide">
          <img
            src="../img/logo-corebiz.png"
            alt="Corebiz logo"
            className="corebiz-logo"
          />
        </div>
        <div className="centerSide">
          <input type="text" placeholder="O que está procurando?" />
          <i class="fas fa-search"></i>
        </div>
        <div className="rigthSide">
          <i class="far fa-user"></i>
          <p>Minha Conta</p>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
