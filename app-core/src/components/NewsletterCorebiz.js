import React from 'react';
import '../App.css';

function NewsletterCorebiz() {
  return (
    <section id="NewsletterCorebiz">
      <div>
        <form action="">
          <h2>Participe de nossas news com promoções e novidades!</h2>
          <div class="Itens">
            <div class="inputNameMail">
              <input
                type="name"
                name=""
                value=""
                placeholder="Digite seu nome"
              />
              <input
                type="email"
                name=""
                value=""
                placeholder="Digite seu email"
              />
            </div>
            <button class="btnNews">Eu quero!</button>
          </div>
        </form>
      </div>
    </section>
  );
}

export default NewsletterCorebiz;
