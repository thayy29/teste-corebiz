import './App.css';
import Navbar from './Components/Navbar.js';
import SectionPrincipal from './Components/SectionPrincipal.js';
import ProductsCorebiz from './Components/ProductsCorebiz.js';
import NewsletterCorebiz from './Components/NewsletterCorebiz.js';
import FooterCorebiz from './Components/FooterCorebiz.js';

function App() {
  return (
    <div className="App">
      <Navbar />
      <SectionPrincipal />
      <ProductsCorebiz />
      <NewsletterCorebiz />
      <FooterCorebiz />
    </div>
  );
}

export default App;
